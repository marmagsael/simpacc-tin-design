<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Sales Module

// Customer
Route::resource('/sales/customer', 'CustomerController');
Route::get('/customer/import-from-google', 'CustomerController@importFromGoogle');
Route::get('/customer/import-from-csv', 'CustomerController@importFromCSV');
// End Customer

//Products and Services
Route::resource('/sales/products-and-services', 'ProductsAndServicesSalesController');
//End Products and Services

//Invoice
Route::resource('/sales/invoices', 'InvoiceController');
//End Invoice

// Customer Statements
Route::get('/sales/customer-statements', function () {
    return view('sales.customerStatements.csList');
});
// End Customer Statements

// Purchases Module

// Vendors
Route::resource('/purchases/vendors', 'VendorController');
Route::get('/vendors/import-from-google', 'VendorController@importFromGoogle');
Route::get('/vendors/import-from-csv', 'VendorController@importFromCSV');
// End Vendors

//Products and Services
Route::resource('/purchases/products-and-services', 'ProductsAndServicesPurchases');
//End Products and Services

//Bills
Route::resource('/purchases/bills', 'BillsController');
// End Bills

//Receipts
Route::resource('/purchases/receipts', 'ReceiptsController');
// End Receipts

//End Purchases Module

//Accounting Module

//Transaction
Route::get('/transaction', function () {
    return view('accounting.transactions.home');
});
//End Transaction

//Reconciliation
Route::resource('/accounting/reconciliation', 'ReconciliationController');
//End Reconciliation

//Chart of Accounts
Route::resource('/accounting/chart-of-accounts', 'ChartOfAccountsController');
//End Chart of ACcounts

//Reports Module
Route::resource('/reports', 'ReportsController');

Route::get('/balance', function () {
    return view('reports.submodules.balanceSheet');
});



