@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row mt-5 mb-5">
        <div class="col-lg-6 col-md-6 col-sm-6">
        <!-- INSERT PIC HERE -->
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="column">
                <p class="lead font-weight-bold">COMING SOON: CUSTOMER STATEMENTS</p>
            </div>
            <div class="column">
                <p class="h1 font-weight-bold">Keep your customers in the loop</p>
            </div>
            <div class="column">
                <p class="lead">Remind your customers about outstanding<br>
                invoices or send details of their acoount <br>
                activity</p>
            </div>
        </div>
    </div>

    <div class="card mt-5">
        <div class="card-body ml-5">
            <div class="column">
                <div class="row">
                    <i class="fa fa-check"></i>
                    <p class="ml-2"><b>Get paid faster.</b> When your customers know what they owe, you get paid.</p>
                </div>
                <div class="row">
                    <i class="fa fa-check"></i>
                    <p class="ml-2"><b>Save time.</b>Instead of resending invoices, show customers everything in one place.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

