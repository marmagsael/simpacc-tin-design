@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/invoice/style.css') }}" rel="stylesheet">
@endsection

@section('page-content')
    <div class="container">
        <!--- HEADER --->
        <div class="d-flex flex-row">
            <h1>New Invoice</h1>
            <div class="ml-auto">
                <div class="btn-group">
                    <button class="button btn btn-primary font-weight-bold btn-rounded dropdown-toggle px-4" data-toggle="dropdown">Save and Continue  </button>
                    <div class="dropdown-menu">
                        <a href="" class="dropdown-item">Save and Send..</a>
                        <a href="" class="dropdown-item">Save and Record a Payment</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- HEADER -->

        <!--- ACCORDION --->
        <div class="accordion" id="BusinessAccordion">
            <div class="card-header" id="header1">
                <div class="row">
                    <div class="col-lg-6 ml-auto flex-column">
                        <div class="d-flex flex-row">
                            <p class="lead">
                                Business address and contact details, title, summary, and logo.
                            </p>
                        </div>
                    </div>    
                    <div class="col-lg-6 flex-column">
                        <div class="d-flex flex-row justify-content-end">
                            <i href="drop" class="fa fa-angle-down" data-toggle="collapse" data-target="#AccordionContent" aria-expanded="true" aria-controls="AccordionContent"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF ACCORDION-->

        <!-- Invoice Details -->
        <div id="AccordionContent" class="collapse show" aria-labelledby="header1" data-parent="#BusinessAccordion">
            <!-- ACCORDION CARD BODY-->
            <div class="card-body">
                <div class="row">
                    <!-- ADD LOGO -->
                    <div class="col-lg-6 col-md-6 col-sm-6 flex-column mb-3">
                        <a href="#" class="card border-info dashed ml-2 invoice-mini-card">
                            <div class="card-body text-center mt-5">
                                <i class="fa fa-upload fa-lg" style="color: blue;"></i>
                                <p>Browse or add your logo here.</p>
                            </div>
                        </a>
                    </div>
                    <!-- END ADD LOGO -->

                    <!-- FORM INVOICE -->
                    <div class="col-lg-6 col-md-6 col-sm-6 flex-column">
                        <form class="justify-content-end">
                            <div class="form-group row">
                                <div class="col-sm-10 ml-auto pr-0">
                                    <input type="text" class="form-control text-right py-4" id="invoiceInput" value="Invoice">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 ml-auto pr-0">
                                    <input type="text" class="form-control text-right " id="sumary" 
                                    placeholder="Summary(e.g. project name, description of invoice)">
                                </div>
                            </div>
                        </form>
                        <div class="mt-3">
                            <p class="text-right"><strong>Company Name</strong></p>
                            <p class="text-right">Company Location</p>
                            <a href="" data-toggle="modal" data-target="#editBusinessModal"><p class="text-right"> Edit your business address and contact details</p></a>
                        </div>
                    </div>
                    <!-- END OF FORM INVOICE -->
                </div>
            </div>
            <!-- END OF ACCORDION CARD BODY -->
        </div>
        <!-- End of Invoice Details -->  
    
        <!-- CARD CONTENT FOR CUSTOMER SECTION-->
        <div class="card mt-2">

            <!-- ADD CUSTOMER SECTION -->
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 flex-column mb-3">
                        <a href="/sales/customer/create" class="card border-info invoice-mini-card">
                            <div class="card-body text-center mt-5">
                                <i class="fa fa-user-plus fa-lg" style="color: blue;"></i>
                                <p>Add a Customer</p>
                            </div>
                        </a>
                    </div>

                    <!-- FORM GROUP -->
                    <div class="col-lg-6 col-lg-6 col-md-6 col-sm-6 flex-column">
                        <form class="justify-content-end">
                            <div class="form-group row">
                                <p class="col lead text-right">Invoice number</p>
                                <div class="col">
                                    <input type="text" class="form-control" id="Invoicenumber">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="col lead text-right">P.O/S.O number</p>
                                <div class="col">
                                    <input type="text" class="form-control" id="POnumber">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="col lead text-right">Invoice date</p>
                                <div class="col">
                                    <input type="date" class="form-control" id="POnumber">
                                </div>
                            </div>
                            <div class="form-group row">
                                <p class="col lead text-right">Payment due</p>
                                <div class="col">
                                    <input type="date" class="form-control mb-0" id="POnumber">
                                    <small class="text-muted">On Receipt</small>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END OF FORM GROUP -->

                </div>
            </div>
            <!-- END OF CUSTOMER SECTION -->
            
            <!-- EDIT COLUMN -->
            <ul class="nav nav-tabs card-tabs mt-3">
                <li class="nav-item">
                    <a class="nav-link active ml-5 " href="#" data-toggle="modal" data-target="#editColModal" id="editColumn">
                        <p><strong>Edit Columns</strong></p>
                    </a>
                </li>
            </ul>
            {{-- ADD ITEMS --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Items</th>
                        <th></th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" class="form-control" id="ItemName" placeholder="Enter Item Name"></td>
                        <td><input type="text" class="form-control" id="ItemDescription" placeholder="Enter Item Description"></td>
                        <td><input type="text" class="form-control" id="Qty" placeholder=""></td>
                        <td><input type="text" class="form-control" id="Price" placeholder=""></td>
                        <td><p class="text-right">Php0.00</p></td>    
                    </tr>
                    <tr>
                        <td><a href="#">Edit income account</a></td>
                        <td></td>
                        <td class="text-right"> <label for="Tax">Tax</label></td>
                        <td><input type="text" class="form-control" id="Tax"></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            {{-- END ADD ITEMS --}}

            <!-- TOTAL ITEMS -->
            <div class="card">
                <div class="card-header text-center">
                    <a href="#"><strong>Add an item</strong></a>
                </div>
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="col-lg-5">
                        </div>
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-2">
                            <p>Subtotal</p>
                        </div>
                        <div class="col-lg-2">
                            <p class="ml-5">Php0.00</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div class="col-lg-5">

                        </div>
                        <div class="col-lg-1 text-right">
                            <p><strong>Total</strong></p>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" id="total">
                        </div>
                        
                        <div class="col-lg-2 ml-5 text-right">
                            <p><strong>Php0.00</strong></p>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group">
                        <label for="textarea" class="text-muted">Notes</label>
                        <textarea name="" id="TextArea" rows="3" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <!-- END TOTAL ITEMS -->

        </div>
        <!-- END OF CARD CONTENT FOR CUSTOMER SECTION -->

        <!-- START OF FOOTER -->
        <div class="card mt-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6 ml-auto flex-column">
                        <div class="d-flex flex-row">
                            <p class="h5">
                                <b> Footer</b>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 flex-column">
                        <div class="d-flex flex-row justify-content-end">
                            <i href="drop" class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF FOOTER -->

        <!-- SAVE AND SEND BUTTON -->
        <div class="d-flex flex-row mt-4">
            <div class="ml-auto">
                <div class="btn-group">
                    <button class="button btn btn-primary font-weight-bold btn-rounded
                        dropdown-toggle justify-content-end mb-5" data-toggle="dropdown">Save and Continue  </button>
                    <div class="dropdown-menu">
                        <a href="" class="dropdown-item">Save and Send..</a>
                        <a href="" class="dropdown-item">Save and Record a Payment</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF SAVE AND SEND -->

    </div>

    @include('sales.invoices.include.editColumn')
    @include('sales.invoices.include.editBusiness')
@endsection

@section('script')
    <script src="{{ asset('js/geodatasource-cr.min.js') }}"></script>
    <script src="{{asset('js/Gettext.js')}}"></script>
@endsection