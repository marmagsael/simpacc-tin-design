<!-- EDIT ADDRESS/CONTECT MODAL -->
<div class="modal fade" id="editBusinessModal" role="dialog" aria-labelledby="ModalLabel"
aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="ModalLabel">Edit Business Address and Contact Details</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body justify-content-center">
                <form>
                    <div class="form-group row align-items-center">
                        <label for="company" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Company/Business</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="company">
                        </div>
                    </div>


                    <h4 class="font-weight-bold">Address</h4>

                    <div class="form-group row align-items-center">
                        <label for="addline1" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Address Line 1</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="addline1">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="addline2" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Address Line 2</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="addline2">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="city" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">City</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="city">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="zipcode" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Postal/Zip Code</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="zipcode">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="country" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Country</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <select class="form-control gds-cr" country-data-region-id="gds-cr-three" data-language="en">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="province" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Province/State</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <select class="form-control" id="gds-cr-three">
                            </select>
                        </div>
                    </div>

                    <h4 class="lead font-weight-bold">Contact</h4>

                    <div class="form-group row align-items-center">
                        <label for="phone" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Phone</div></label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="number" class="form-control" id="phone">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="Fax" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Fax</div> </label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="fax">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="Mobile" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Mobile</div> </label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="mobile">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="Tollfree" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Toll-free</div> </label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="Tollfree">
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="Website" class="col-lg-3 col-sm-12 col-form-label text-right">
                            <div class="lead">Website</div> </label>
                        <div class="col-lg-7 col-sm-12">
                            <input type="text" class="form-control" id="Website">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>