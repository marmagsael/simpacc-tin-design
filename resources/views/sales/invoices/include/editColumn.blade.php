<!-- MODAL EDIT COLUMN -->
<div class="modal fade" id="editColModal" role="dialog" aria-labelledby="ModalLabel"
aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="ModalLabel">Customize this Invoice</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <!--ITEMS RADIO BUTTON  -->
            <div class="modal-body">
                <p class="lead">Edit the title of the columns in this Invoice:</p>
                <form>
                    <fieldset class="form-group">
                        <div class="row">
                                <h4 class="col-md-4 col-sm-4 text-center">Items</h4>
                            <div class="col-md-5 col-sm-5">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                                    <label class=" lead custom-control-label" for="customRadio1">Items (Default)</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="lead custom-control-label" for="customRadio2">Products</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                    <label class="lead custom-control-label" for="customRadio3">Services</label>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                            <label class="lead custom-control-label" for="customRadio4">Others</label>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <input type="text" class="form-control" id="others">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END OF ITEMS RADIO BUTTON -->

            <!-- UNIT RADIO BUTTON -->
            <div class="modal-body">
                <form>
                    <fieldset class="form-group">
                        <div class="row">
                                <h4 class=" col-md-4 col-sm-4 text-center">Unit</h4>
                            <div class="col-md-5 col-sm-5">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                                    <label class="lead custom-control-label" for="customRadio1">Quantity (Default)</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="lead custom-control-label" for="customRadio2">Hours</label>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class="lead custom-control-label" for="customRadio3">Others</label>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <input type="text" class="form-control" id="others">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END OF UNIT RADIO BUTTON -->

            <!-- PRICE RADIO BUTTON -->
            <div class="modal-body">
                <form>
                    <fieldset class="form-group">
                        <div class="row">
                                <h4 class=" col-md-4 col-sm-4 text-center">Price</h4>
                            <div class="col-md-5 col-sm-5">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                                    <label class="lead custom-control-label" for="customRadio1">Price (Default)</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="lead custom-control-label" for="customRadio2">Rate</label>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class=" lead custom-control-label" for="customRadio3">Others</label>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <input type="text" class="form-control" id="others">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END OF PRICE RADIO BUTTON -->

            <!-- AMOUNT RADIO BUTTON -->
            <div class="modal-body">
                <form>
                    <fieldset class="form-group">
                        <div class="row">
                                <h4 class=" col-md-4 col-sm-4 text-center">Amount</h4>
                            <div class="col-md-5 col-sm-5">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked>
                                    <label class="lead custom-control-label" for="customRadio1">Amount (Default)</label>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                            <label class=" lead custom-control-label" for="customRadio2">Others</label>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <input type="text" class="form-control" id="others">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END OF AMOUNT RADIO BUTTON -->

            <!-- HIDE COLUMNS -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <h4 class=" ml-3">Hide Columns:</h4>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="lead custom-control-label" for="customCheck1">Hide item name</label>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-5">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="lead custom-control-label" for="customCheck2">Hide item description</label>
                        </div>
                    </div>
                </div>
                        <p class="small text-muted text-center">Your invoice must show at least one of the above.</p>
                    
                <div class="row">
                    <div class="col-md-3 col-sm-3">

                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck3">
                                <label class="lead custom-control-label" for="customCheck3">Hide Units</label>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck4">
                                <label class="lead custom-control-label" for="customCheck4">Hide Price</label>
                            </div>
                        </div>
                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck5">
                                <label class="lead custom-control-label" for="customCheck5">Hide Amount</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF HIDE COLUMNS -->

            <!-- APPLY CHANGES -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">

                    </div>

                    <div class="col-md-9 col-sm-9">
                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck6">
                                <label class="lead custom-control-label" for="customCheck6">
                                    Apply these settings to all future invoices.

                                    <p class="small">These settings will apply to recurring and non-recurring invoices.
                                        You can change these anytime from invoice Customization settings.
                                    </p>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF APPLY CHANGES -->

            <!-- MODAL FOOTER -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
            <!-- END OF MODAL FOOTER -->
        </div>
    </div>
</div>
<!-- END OF MODAL EDIT COLUMN -->