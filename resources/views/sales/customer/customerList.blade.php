@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row">
        <div class="col-lg-6 col-md-4">
            <h1>Customers</h1>
        </div>
        
        <div class="col-lg-6 col-md-8 d-sm-block">
            <div class="text-lg-right text-md-right text-sm-center button-header" >
                {{-- Import button --}}
                <div class="btn-group">
                    <button class="btn btn-white font-weight-bold btn-rounded px-4 mx-2 dropdown-toggle" data-flip="false" data-toggle="dropdown">Import from...</button>    
                    <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="/customer/import-from-csv">CSV</a>
                            <a class="dropdown-item" href="/customer/import-from-google">Google Contacts</a>
                    </div>
                </div>

                {{-- Add customer button --}}
                <a href="/sales/customer/create" class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Add a customer</a>    
            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- Customer list --}}
    <div class="my-4">
        
        {{-- @if(table is not empty) --}}

        {{-- @else --}}
            <p class="font-italic">You haven't added any customers yet.</p>
        {{-- @endif --}}  

    </div>{{-- Customer list --}}
    
</div>
@endsection