@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="d-flex flex-column">

        <h1>Import Customers from Google</h1>
        <p class="lead">If you use Google email for your business, you probably have your contacts stored inside your Google account. 
            You can import them into your Wave business.
        </p>
            
    </div>{{-- Header --}}

    <div class="row">
        <div class="col-lg-6 col-md-4 col-sm-3 my-5">
            <a href="" class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Import from Google Contacts</a>
        </div>    
    </div>
    
    
    
</div>
@endsection