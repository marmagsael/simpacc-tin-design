<!-- Modal Content -->
<div class="modal fade" id="ESBalance" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="ESBalLabel">Ending Statement Balance</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <!-- Modal Header -->
            <!-- Modal Body -->
            <div class="modal-body ml-3">
                <p class="font-weight-bold mb-4">Cash on hand</p>
                <p class="mb-4">Enter the ending balance as it appears on your statement</p>
                    <div class="row mb-4">
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <p>Ending Balance Date</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <input type="date" class="form-control">
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1">
                        
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <p>Ending Balance Account</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <input type="text" class="form-control" placeholder="Php">
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1">
                        
                        </div>
                    </div>
            </div>
            <!-- Modal Body -->
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-light btn-rounded" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-rounded">Save</button>
            </div>
            <!--Modal Footer  -->
        </div>
    </div>
</div>
<!-- Modal Content -->