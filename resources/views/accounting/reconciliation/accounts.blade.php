@extends('layouts.app')

@section('page-content')
<div class="container">
    <!-- Header -->
        <div class="d-flex flex-row mt-5">
                <h1>Account Reconciliation</h1>
            <div class="ml-auto">
                <div class="btn-group">
                    <a href="#" class="font-weight-bold">Watch Video</a>
                </div>
            </div>
        </div>
    <!-- Header -->

    <!-- Card -->
    <div class="card mt-5">
        <div class="card-header">
            <p class="lead font-weight-bold">Manually added accounts</p>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <p class="lead">Cash on Hand</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 text-right ">
                    <p class="font-weight-bold">Php0.00</p>
                </div>
            </div>
            <!-- Show modal -->
            <div class="text-right">
                <button class="button btn btn-primary font-weight-bold btn-rounded"
                    data-toggle="modal" data-target="#ESBalance"> 
                    Get started  
                </button>
            </div>
            <!-- SHow modal -->
        </div>
        {{-- Ending statement balance modal --}}
        @include('accounting.reconciliation.include.endingStatement');
        {{-- Ending statement balance modal --}}
    </div>
    <!-- Card -->
</div>
@endsection