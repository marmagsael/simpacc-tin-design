@extends('layouts.app')

@section('style')
  <link href="{{ asset('css/chartOfAccounts/style.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/bootstrapSelect/bootstrap-select.css') }}">
@endsection

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <h1>Chart of Accounts</h1>
        </div>
        <div class="col-lg-6 col-md-6 d-sm-block">
            <div class="text-lg-right text-md-right text-sm-center button-header">

                {{-- Add products and services button --}}
                <button class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2" data-toggle="modal" data-target="#addAccountModal">Add a new account</button>

            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- Sub navigation --}}
    <ul class="nav nav-pills mt-4 lead">
        <li class="nav-item">
          <a class="nav-link active" href="#assetSection" data-toggle="pill">Assets</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#liabilitiesSection" data-toggle="pill">Liabilities & Credit Cards</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#incomeSection" data-toggle="pill">Income</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#expensesSection" data-toggle="pill">Expenses</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#equitySection" data-toggle="pill">Equity</a>
        </li>
    </ul>{{-- Sub navigation --}}

    <hr>

    {{-- Main Content --}}
    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="assetSection">
        @include('accounting.chartOfAccounts.include.assets')
      </div>
      <div class="tab-pane fade" id="liabilitiesSection">
        @include('accounting.chartOfAccounts.include.liabilities')
      </div>
      <div class="tab-pane fade" id="incomeSection">
        @include('accounting.chartOfAccounts.include.income')
      </div>
      <div class="tab-pane fade" id="expensesSection">
        @include('accounting.chartOfAccounts.include.expenses')
      </div>
      <div class="tab-pane fade" id="equitySection">
        @include('accounting.chartOfAccounts.include.equity')
      </div>
    </div>{{-- Main content --}}
    
</div>

@include('accounting.chartOfAccounts.include.addAccount')
@endsection

@section('script')
  <script src="{{ asset('js/bootstrapSelect/bootstrap-select.js') }}"></script>
  <script src="{{ asset('js/accounting/chartOfAccounts/script.js') }}"></script>
@endsection