
{{-- Operating Expense --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Operating Expense</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Accounting Fees</p>
                No transactions for this account
            </td>
            <td class="pr-5">Accounting or bookkeeping services for your business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5"> 
                <p>Advertising & Promotion</p>
                No transactions for this account
            </td>
            <td class="pr-5">Advertising or other costs to promote your business. Includes web or social media promotion.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Bank Service Charges</p>
                No transactions for this account
            </td>
            <td class="pr-5"> Fees you pay to your bank like transaction charges, monthly charges, and overdraft charges.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Computer – Hardware</p>
                No transactions for this account
            </td>
            <td class="pr-5">Desktop or laptop computers, mobile phones, tablets, and accessories used for your business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Computer – Hosting</p>
                No transactions for this account
            </td>
            <td class="pr-5">Fees for web storage and access, like hosting your business website or app.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Computer – Internet</p>
                No transactions for this account
            </td>
            <td class="pr-5">Internet services for your business. Does not include data access for mobile devices.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Computer – Software</p>
                No transactions for this account
            </td>
            <td class="pr-5">Apps, software, and web or cloud services you use for business on your mobile phone or computer. Includes one-time purchases and subscriptions.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5"> 
                <p>Depreciation Expense</p>
                No transactions for this account
            </td>
            <td class="pr-5"></td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Dues & Subscriptions</p>
                No transactions for this account
            </td>
            <td class="pr-5">Fees or dues you pay to professional, business, and civic organizations. Does not include business licenses and permits or business memberships.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Education & Training</p>
                No transactions for this account
            </td>
            <td class="pr-5">Education and training for yourself or your staff that directly relates to your business, including annual courses to maintain a professional designation, or required safety certifications.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Insurance – General Liability</p>
                No transactions for this account
            </td>
            <td class="pr-5">Premiums that insure your business for things like general liability or workers compensation.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Insurance – Vehicles</p>
                No transactions for this account
            </td>
            <td class="pr-5">Insurance for the vehicle you use for business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Interest Expense</p>
                No transactions for this account
            </td>
            <td class="pr-5">Interest your business pays on loans and other forms of debt, including business loans, credit cards, mortgages, and vehicle payments.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Meals and Entertainment</p>
                No transactions for this account
            </td>
            <td class="pr-5">Food and beverages you consume while conducting business, with clients and vendors, or entertaining customers.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Office Supplies</p>
                No transactions for this account
            </td>
            <td class="pr-5">Office supplies and services for your business office or space.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Professional Fees</p>
                No transactions for this account
            </td>
            <td class="pr-5">Fees you pay to consultants or trained professionals for advice or services related to your business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Rent Expense</p>
                No transactions for this account
            </td>
            <td class="pr-5">Costs to rent or lease property or furniture for your business office space. Does not include equipment rentals.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Repairs & Maintenance</p>
                No transactions for this account
            </td>
            <td class="pr-5">Repair and upkeep of property or equipment, as long as the repair doesn't add value to the property. Does not include replacements or upgrades.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Telephone – Land Line</p>
                No transactions for this account
            </td>
            <td class="pr-5">Land line phone services for your business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Telephone – Wireless</p>
                No transactions for this account
            </td>
            <td class="pr-5">Mobile phone services for your business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Travel Expense</p>
                No transactions for this account
            </td>
            <td class="pr-5">Transportation and travel costs while traveling for business. Does not include daily commute costs.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Utilities</p>
                No transactions for this account
            </td>
            <td class="pr-5">Utilities (electricity, water, etc.) for your business office. Does not include phone use.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Vehicle – Fuel</p>
                No transactions for this account
            </td>
            <td class="pr-5">Gas and fuel costs when driving for business.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5">
                <p>Vehicle – Repairs & Maintenance</p>
                No transactions for this account
            </td>
            <td class="pr-5">Repairs and preventative maitenance of the vehicle you drive for business.</td>
        </tr>
    </tbody>
</table>{{-- Operating Expense --}}

{{-- Cost of Goods Sold --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Cost of Goods Sold</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Cost of Goods Sold accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Cost of Goods Sold --}}

{{-- Payment Processing Fee --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Payment Processing Fee</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Payment Processing Fee accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Payment Processing Fee --}}

{{-- Payroll Expense --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Payroll Expense</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Payroll – Employee Benefits</p>No transactions for this account</td>
            <td class="pr-5">
                Federal and provincial/state deductions taken from an employee's pay, like employment insurance. 
                These are usually described as line deductions on the pay stub.
            </td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5"><p>Payroll – Employer's Share of Benefits</p>No transactions for this account</td>
            <td class="pr-5">
                The portion of federal and provincial/state obligations your business is responsible for paying as an employer.
            </td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5"><p>Payroll – Salary & Wages</p>No transactions for this account</td>
            <td class="pr-5">
                Wages and salaries paid to your employees.
            </td>
        </tr>
    </tbody>
</table>{{-- Payroll Expense --}}

{{-- Uncategorized Expense --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Uncategorized Expense</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Uncategorized Expense</p>No transactions for this account</td>
            <td>A business cost you haven't categorized yet. Categorize it now to keep your records accurate.</td>
        </tr>
    </tbody>
</table>{{-- Uncategorized Expense --}}

{{-- Loss On Foreign Exchange --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Loss On Foreign Exchangee</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Loss on Foreign Exchange</p>No transactions for this account</td>
            <td class="pr-5 text-justify">Foreign exchange losses happen when the exchange rate between your business's home currency 
                and a foreign currency transaction changes and results in a loss. This can happen in the time
                between a transaction being entered in Wave and being settled, for example, between when you 
                send an invoice and when your customer pays it. This can affect foreign currency invoice payments,
                bill payments, or foreign currency held in your bank account.	</td>
        </tr>
    </tbody>
</table>{{-- Loss On Foreign Exchange --}}





