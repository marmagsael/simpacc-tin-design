
{{-- Business Owner Contribution and Drawing --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Business Owner Contribution and Drawing</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Owner Investment / Drawings</p>No transactions for this account</td>
            <td class="pr-5">Owner investment represents the amount of money or assets you put into your 
                business, either to start the business or keep it running. An owner's draw 
                is a direct withdrawal from business cash or assets for your personal use.</td>
        </tr>
    </tbody>
</table>{{-- Business Owner Contribution and Drawing --}}

{{-- Retained Earnings: Profit --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Retained Earnings: Profit</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Owner's Equity</p>No transactions for this account</td>
            <td class="pr-5">Owner's equity is what remains after you subtract business liabilities from business assets. <br> 
                In other words, it's what's left over for you if you sell all your assets and pay all your debts.</td>
        </tr>
    </tbody>
</table>{{-- Retained Earnings: Profit --}}
