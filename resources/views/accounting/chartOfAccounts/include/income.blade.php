
{{-- Income --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Income</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Consulting Income</p> No transactions for this account.</td>
            <td class="pr-5">Money your business receives by providing consulting services to your customers.</td>
        </tr>
        <tr>
            <td class="col-left border-right pl-5"><p>Sales</p> No transactions for this account.</td>
            <td class="pr-5">Payments from your customers for products and services that your business sold.</td>
        </tr>
    </tbody>
</table>{{-- Income --}}

{{-- Discount --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Discount</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Discount accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Discount --}}

{{-- Other Income --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Other Income</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Other Income accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Other Income --}}

{{-- Uncategorized Income --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Uncategorized Income</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Uncategorized Income</p> No transactions for this account.</td>
            <td>Income you haven't categorized yet. Categorize it now to keep your records accurate.</td>
        </tr>
    </tbody>
</table>{{-- Uncategorized Income --}}

{{-- Gain On Foreign Exchange --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Gain On Foreign Exchange</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-left border-right pl-5"><p>Gain On Foreign Exchange</p>No transactions for this account</td>
            <td class="pr-5 text-justify">
                Foreign exchange gains happen when the exchange rate between your business's home currency
                and a foreign currency transaction changes and results in a gain.
                This can happen in the time between a transaction being entered in Wave and being settled,
                for example, between when you send an invoice and when your customer pays it.
                This can affect foreign currency invoice payments, bill payments, or foreign currency held in your bank account.
            </td>
        </tr>
    </tbody>
</table>{{-- Gain On Foreign Exchange --}}
