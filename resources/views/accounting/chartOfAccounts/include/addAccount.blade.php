<div class="modal fade" id="addAccountModal">
    <form action="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Add an account</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5">
                    {{-- Account type --}}
                    <div class="row form-group">
                        <div class="col-md-5 text-right lead">
                            Account type <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-7">
                            <select class="selectpicker form-control"  data-live-search="true" title="Select one..." id="selectAccountType">
                                <optgroup label="Assets">
                                        <option value="Cash and Bank">Cash and Bank</option>
                                        <option value="Money in Transit">Money in Transit</option>
                                        <option value="Expected Payments from Customers">Expected Payments from Customers</option> {{-- no need for account currency --}}
                                        <option value="Inventory">Inventory</option>
                                        <option value="Property, Plant, Equipment">Property, Plant, Equipment</option>
                                        <option value="Depreciation and Amortization">Depreciation and Amortization</option> {{-- no need for account currency --}}
                                        <option value="Vendor Prepayments and Vendor Credits">Vendor Prepayments and Vendor Credits</option>
                                        <option value="Other Short-Term Asset">Other Short-Term Asset</option>
                                        <option value="Other Short-Term Asset">Other Short-Term Asset</option>
                                </optgroup>
                                <optgroup label="Liabilities & Credit Cards">
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Loan and Line of Credit">Loan and Line of Credit</option>
                                        <option value="Expected Payments to Vendors">Expected Payments to Vendors</option>
                                        <option value="Due For Payroll">Due For Payroll</option> {{-- no need for account currency --}}
                                        <option value="Due to You and Other Business Owners">Due to You and Other Business Owners</option>
                                        <option value="Customer Prepayments and Customer Credits">Customer Prepayments and Customer Credits</option>
                                        <option value="Other Short-Term Liability">Other Short-Term Liability</option>
                                        <option value="Other Long-Term Liability">Other Long-Term Liability</option>
                                </optgroup>
                                <optgroup label="Income">
                                        <option value="Income">Income</option> {{-- no need for account currency --}}
                                        <option value="Discount">Discount</option> {{-- no need for account currency --}}
                                        <option value="Other income">Other income</option> {{-- no need for account currency --}}
                                </optgroup>
                                <optgroup label="Expenses">
                                        <option value="Operating Expense">Operating Expense</option> {{-- no need for account currency --}}
                                        <option value="Cost of Goods Sold">Cost of Goods Sold</option> {{-- no need for account currency --}}
                                        <option value="Payment Processing Fee">Payment Processing Fee</option> {{-- no need for account currency --}}
                                        <option value="Payroll Expense">Payroll Expense</option> {{-- no need for account currency --}}
                                </optgroup>
                                <optgroup label="Equity">
                                        <option value="Business Owner Contribution and Drawing">Business Owner Contribution and Drawing</option>
                                        <option value="Retained Earnings: Profit">Retained Earnings: Profit</option> {{-- no need for account currency --}}
                                </optgroup>
                            </select>
                                      
                        </div>
                    </div>{{-- Account type --}}

                    {{-- Account name --}}
                    <div class="row form-group">
                        <div class="col-md-5 text-right lead">
                            Account name <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-7">
                            <input type="text" class="form-control">
                        </div>
                    </div>{{-- Account name --}}

                    {{-- Account currency --}}
                    <div class="row form-group" id="inputCurrency">
                        <div class="col-md-5 text-right lead">
                            Account currency
                        </div>
                        <div class="col-md-7">
                                <select class="form-control">
                                        <option value="">---------</option>
                                        <option value="192">AED - UAE dirham</option>
                                        <option value="1">AFN - Afghani</option>
                                        <option value="2">ALL - Lek</option>
                                        <option value="10">AMD - Armenian dram</option>
                                        <option value="135">ANG - Netherlands Antillean Guilder</option>
                                        <option value="6">AOA - Kwanza</option>
                                        <option value="9">ARS - Argentinian peso</option>
                                        <option value="12">AUD - Australian dollar</option>
                                        <option value="11">AWG - Aruban Guilder</option>
                                        <option value="14">AZN - New Manat</option>
                                        <option value="27">BAM - Convertible Marks</option>
                                        <option value="18">BBD - Barbados dollar</option>
                                        <option value="17">BDT - Taka</option>
                                        <option value="31">BGN - Lev</option>
                                        <option value="16">BHD - Bahraini dinar</option>
                                        <option value="33">BIF - Burundi franc</option>
                                        <option value="23">BMD - Bermuda dollar</option>
                                        <option value="30">BND - Brunei dollar</option>
                                        <option value="26">BOB - Boliviano</option>
                                        <option value="29">BRL - Real</option>
                                        <option value="15">BSD - Bahamian dollar</option>
                                        <option value="24">BTN - Ngultrum</option>
                                        <option value="28">BWP - Pula</option>
                                        <option value="19">BYR - Belarussian rouble</option>
                                        <option value="21">BZD - Belize dollar</option>
                                        <option value="36">CAD - Canadian dollar</option>
                                        <option value="46">CDF - Franc congolais</option>
                                        <option value="110">CHF - Swiss franc</option>
                                        <option value="41">CLP - Chilean peso</option>
                                        <option value="42">CNY - Ren-Min-Bi yuan</option>
                                        <option value="43">COP - Colombian peso</option>
                                        <option value="47">CRC - Costa Rican colon</option>
                                        <option value="50">CUP - Cuban peso</option>
                                        <option value="37">CVE - Cape Verde escudo</option>
                                        <option value="52">CZK - Czech koruna</option>
                                        <option value="54">DJF - Djibouti franc</option>
                                        <option value="53">DKK - Danish krone</option>
                                        <option value="56">DOP - Dominican peso</option>
                                        <option value="3">DZD - Algerian dinar</option>
                                        <option value="63">EEK - Estonian kroon</option>
                                        <option value="58">EGP - Egyptian pound</option>
                                        <option value="62">ERN - Nakfa</option>
                                        <option value="64">ETB - Ethiopian birr</option>
                                        <option value="125">EUR - Euro</option>
                                        <option value="65">FJD - Fiji dollar</option>
                                        <option value="203">FKP - Falkland Islands (Malvinas) Pound</option>
                                        <option value="193">GBP - Pound sterling</option>
                                        <option value="71">GEL - Lari</option>
                                        <option value="73">GHS - Ghana Cedi</option>
                                        <option value="74">GIP - Gibraltar pound</option>
                                        <option value="70">GMD - Dalasi</option>
                                        <option value="78">GNF - Guinean franc</option>
                                        <option value="77">GTQ - Quetzal</option>
                                        <option value="80">GWP - Guinean bissau Peso</option>
                                        <option value="81">GYD - Guyana dollar</option>
                                        <option value="85">HKD - Hong Kong dollar</option>
                                        <option value="84">HNL - Lempira</option>
                                        <option value="49">HRK - Kuna</option>
                                        <option value="82">HTG - Haitian gourde</option>
                                        <option value="86">HUF - Forint</option>
                                        <option value="89">IDR - Rupiah</option>
                                        <option value="93">ILS - New Israeli sheqel</option>
                                        <option value="25">INR - Indian rupee</option>
                                        <option value="91">IQD - Iraqi dinar</option>
                                        <option value="90">IRR - Iranian rial</option>
                                        <option value="87">ISK - Icelandic Krona</option>
                                        <option value="95">JMD - Jamaican dollar</option>
                                        <option value="97">JOD - Jordanian dinar</option>
                                        <option value="96">JPY - Yen</option>
                                        <option value="99">KES - Kenyan shilling</option>
                                        <option value="102">KGS - Kyrgyz Som</option>
                                        <option value="34">KHR - Riel</option>
                                        <option value="44">KMF - Comoro franc</option>
                                        <option value="100">KRW - Won</option>
                                        <option value="101">KWD - Kuwaiti dinar</option>
                                        <option value="38">KYD - Cayman Islands dollar</option>
                                        <option value="98">KZT - Tenge</option>
                                        <option value="103">LAK - Kip</option>
                                        <option value="105">LBP - Lebanese pound</option>
                                        <option value="171">LKR - Sri Lankan rupee</option>
                                        <option value="108">LRD - Liberian dollar</option>
                                        <option value="106">LSL - Loti</option>
                                        <option value="111">LTL - Lithuanian litus</option>
                                        <option value="104">LVL - Latvian lats</option>
                                        <option value="109">LYD - Libyan dinar</option>
                                        <option value="128">MAD - Moroccan dirham</option>
                                        <option value="124">MDL - Moldovan leu</option>
                                        <option value="114">MGA - Malagasy Ariary</option>
                                        <option value="67">MKD - Denar</option>
                                        <option value="130">MMK - Kyat</option>
                                        <option value="126">MNT - Tugrik</option>
                                        <option value="113">MOP - Pataca</option>
                                        <option value="121">MRO - Ouguiya</option>
                                        <option value="211">MRU - Ouguiya</option>
                                        <option value="122">MUR - Mauritian rupee</option>
                                        <option value="117">MVR - Rufiyaa</option>
                                        <option value="115">MWK - Kwacha</option>
                                        <option value="123">MXN - Mexican peso</option>
                                        <option value="116">MYR - Malaysian ringgit</option>
                                        <option value="129">MZN - Metical</option>
                                        <option value="131">NAD - Namibian dollar</option>
                                        <option value="139">NGN - Naira</option>
                                        <option value="137">NIO - Cordoba Oro</option>
                                        <option value="140">NOK - Norwegian krone</option>
                                        <option value="134">NPR - Nepalese rupee</option>
                                        <option value="136">NZD - New Zealand dollar</option>
                                        <option value="141">OMR - Omani rial</option>
                                        <option value="143">PAB - Balboa</option>
                                        <option value="147">PEN - Nuevo Sol</option>
                                        <option value="145">PGK - Kina</option>
                                        <option value="148" selected="selected">PHP - Philippine peso</option>
                                        <option value="142">PKR - Pakistani rupee</option>
                                        <option value="149">PLN - Zloty</option>
                                        <option value="146">PYG - Guarani</option>
                                        <option value="152">QAR - Qatari riyal</option>
                                        <option value="153">RON - New Leu</option>
                                        <option value="162">RSD - Serbian Dinar</option>
                                        <option value="154">RUB - Russian rouble</option>
                                        <option value="155">RWF - Rwanda franc</option>
                                        <option value="160">SAR - Saudi riyal</option>
                                        <option value="208">SBD - Solomon Islands Dollar</option>
                                        <option value="163">SCR - Seychelles rupee</option>
                                        <option value="172">SDG - Sudanese Pound</option>
                                        <option value="175">SEK - Swedish Krona</option>
                                        <option value="165">SGD - Singapore dollar</option>
                                        <option value="207">SHP - Saint Helena Pound</option>
                                        <option value="164">SLL - Leone</option>
                                        <option value="168">SOS - Somali shilling</option>
                                        <option value="173">SRD - Surinam dollar</option>
                                        <option value="212">SSP - South Sudanese pound</option>
                                        <option value="159">STD - Dobra</option>
                                        <option value="59">SVC - El Salvador colon</option>
                                        <option value="177">SYP - Syrian pound</option>
                                        <option value="174">SZL - Lilangeni</option>
                                        <option value="180">THB - Baht</option>
                                        <option value="178">TJS - Somoni</option>
                                        <option value="188">TMM - Manat</option>
                                        <option value="186">TND - Tunisian dinar</option>
                                        <option value="184">TOP - Pa&#39;anga</option>
                                        <option value="187">TRY - Turkish Lira</option>
                                        <option value="185">TTD - Trinidad and Tobago dollar</option>
                                        <option value="206">TWD - Taiwan New Dollar</option>
                                        <option value="179">TZS - Tanzanian shilling</option>
                                        <option value="191">UAH - Hryvnia</option>
                                        <option value="190">UGX - Ugandan shilling</option>
                                        <option value="199">USD - U.S. dollar</option>
                                        <option value="195">UYU - Uruguayo peso</option>
                                        <option value="196">UZS - Uzbekistan sum</option>
                                        <option value="197">VEF - Bolivar Fuerte</option>
                                        <option value="198">VND - Dong</option>
                                        <option value="205">VUV - Vatu</option>
                                        <option value="209">WST - Samoan Tala</option>
                                        <option value="35">XAF - CFA Franc - BEAC</option>
                                        <option value="127">XCD - Eastern Caribbean dollar</option>
                                        <option value="138">XOF - CFA franc - BCEAO</option>
                                        <option value="204">XPF - Comptoirs Francais du Pacifique Francs</option>
                                        <option value="200">YER - Yemeni rial</option>
                                        <option value="132">ZAR - Rand</option>
                                        <option value="201">ZMK - Kwacha</option>
                                        <option value="210">ZMW - Kwacha</option>
                                        <option value="202">ZWD - Zimbabwean dollar</option>
                                </select>
                        </div>
                    </div>{{-- Account currency --}}

                    {{-- Account ID --}}
                    <div class="row form-group">
                        <div class="col-md-5 text-right lead">
                            Account ID
                        </div>
                        <div class="col-md-5 mr-auto">
                            <input type="text" class="form-control">
                        </div>
                    </div>{{-- Account ID --}}

                    {{-- Description --}}
                    <div class="row form-group">
                        <div class="col-md-5 text-right lead">
                            Description
                        </div>
                        <div class="col-md-7 mr-auto">
                            <textarea rows="7" class="form-control"></textarea>
                        </div>
                    </div>{{-- Description --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>    
    </form>
  
</div>