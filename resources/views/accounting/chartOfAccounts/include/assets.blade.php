
{{-- Cash and Bank --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Cash and Bank</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="border-right col-left pl-5"><p>Cash on Hand</p>No transactions for this account.</td>
            <td class="pr-5">
                Cash you haven’t deposited in the bank. Add your bank and credit card accounts
                to accurately categorize transactions that aren't cash.
            </td>
        </tr>
    </tbody>
</table>{{-- Cash and Bank --}}

{{-- Money in Transit --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Money in Transit</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Money in Transit accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Money in Transit --}}

{{-- Expected Payments from Customers --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Expected Payments from Customers</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="border-right col-left pl-5"><p>Accounts Receivable</p> No transactions for this account.</td>
        </tr>
    </tbody>
</table>{{-- Expected Payments from Customers --}}

{{-- Inventory --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Inventory</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Inventory accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Inventory --}}

{{-- Property, Plant, Equipment --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Property, Plant, Equipment</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Property, Plant, Equipment accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Property, Plant, Equipment --}}

{{-- Depreciation and Amortization --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Depreciation and Amortization</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Depreciation and Amortization accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Depreciation and Amortization --}}

{{-- Vendor Prepayments and Vendor Credits --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Vendor Prepayments and Vendor Credits</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Vendor Prepayments and Vendor Credits accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Vendor Prepayments and Vendor Credits --}}

{{-- Other Short-Term Asset --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Other Short-Term Asset</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Other Short-Term Asset accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Other Short-Term Asset --}}

{{-- Other Long-Term Asset --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Other Long-Term Asset</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic border-right col-left pl-5">You haven't added any Other Long-Term Asset accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Other Long-Term Asset --}}
