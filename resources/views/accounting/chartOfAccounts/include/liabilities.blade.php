
{{-- Credit Card --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Credit Card</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Credit Card accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Credit Card --}}

{{-- Loan and Line of Credit --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Loan and Line of Credit</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Loan and Line of Credit accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Loan and Line of Credit --}}

{{-- Expected Payments to Vendors --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Expected Payments to Vendors</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="pl-5"><p>Accounts Payable</p> No transactions for this account</td>
        </tr>
    </tbody>
</table>{{-- Expected Payments to Vendors --}}

{{-- Sales Taxes --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Sales Taxes</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Sales Taxes accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Sales Taxes --}}

{{-- Due For Payroll --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Due For Payroll</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Due For Payroll accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Due For Payroll --}}

{{-- Due to You and Other Business Owners --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Due to You and Other Business Owners</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Due to You and Other Business Owners accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Due to You and Other Business Owners --}}

{{-- Customer Prepayments and Customer Credits --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Customer Prepayments and Customer Credits</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Customer Prepayments and Customer Credits accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Customer Prepayments and Customer Credits --}}

{{-- Other Short-Term Liability --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Other Short-Term Liability</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Other Short-Term Liability accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Other Short-Term Liability --}}

{{-- Other Long-Term Liability --}}
<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th class="lead">Other Long-Term Liability</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-italic pl-5">You haven't added any Other Long-Term Liability accounts yet.</td>
        </tr>
    </tbody>
</table>{{-- Other Long-Term Liability --}}


