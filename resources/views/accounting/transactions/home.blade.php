@extends('layouts.app')

@section('page-content')
    <!-- Container -->
    <div class="container">
        <!-- Read Article -->
        <div class="card mt-5 border-warning py-0">
            <div class="card-body">
                <div class="row ml-3">
                    <i class="fa fa-exclamation-triangle fa-2x" style="color: green;"></i>
                    <p class="font ml-3" style="color: green;">
                        Bank connections will not be available after December 31, 2019.
                        <a href="#" style="color: green;" class="font-weight-bold"><u>Read our article</u></a> for more information.
                    </p>
                </div>
            </div>
        </div>
        <!-- Read Article -->

        <!-- All Accounts Row -->
        <div class="row mt-2">  
            <div class="col-lg-2 col-md-2">
                <p class="h1">Transactions</p>
            </div>
            <div class="col-lg-3 col-md-3">

            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 mt-3">
                <div class="d-flex flex-row-reverse">
                    <div class="btn-group">
                        <button class="button btn btn-primary font-weight-bold btn-rounded dropdown-toggle ml-3 mr-2"
                            data-toggle="dropdown">More</button>
                        <div class="dropdown-menu">
                            <a href="" class="dropdown-item">Add Journal Transaction</a>
                            <a href="" class="dropdown-item">Connect your bank</a>
                            <a href="UploadBank.php" class="dropdown-item">Upload a Bank Statement</a>
                        </div>
                    </div>
                    <button class="button btn btn-primary font-weight-bold btn-rounded ml-3">
                        Add Expense
                    </button>
                    <button class="button btn btn-primary font-weight-bold btn-rounded ml-3">
                        Add Income
                    </button>
                    <a href="#" class="popover-test"><i class="fa fa-gift fa-lg mt-3" style="color: black;"> Whats new</i></a>
                </div>
            </div>
        </div>
        <!-- All Accounts Row -->

        <!-- All Accounts Second Column -->
        <div class="btn-group">
            <button class="btn-lg btn-light dropdown-toggle" data-toggle="dropdown">
                All Accounts &nbsp;&nbsp;&nbsp;&nbsp; Php0.00                
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item font-weight-bold">Cash and Bank</a>
                <a href="" class="dropdown-item">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                        Cash on Hand
                        </div>
                        <div class="col-lg-6 col-sm-6 text-right">
                        Php0.00
                        </div>
                    </div>
                </a>
                <div class="dropdown-divider">
                </div>
                    <a href="" class="dropdown-item">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                            All Accounts
                            </div>
                            <div class="col-lg-6 col-sm-6 text-right">
                            Php0.00
                            </div>
                        </div>
                    </a>
                <div class="dropdown-divider">
                </div>
                    <table>
                        <th class="text-center">
                            <a href="">
                                <i class="fa fa-university"> Connect your Bank</i>
                            </a>
                        </th>
                        <th class="text-center">
                            <a href="">
                                <i class="fa fa-plus-circle"> Add a new account</i>
                            </a>
                        </th>
                    </table>
            </div>
        </div>
        <!-- All Accounts Second Column -->

        <!-- Table -->
        <table class="table mt-5">
            <thead>
                <tr>
                    <td scope="col">
                        <input type="checkbox">
                    </td>
                    <td scope="col">Select All</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td scope="col">Filter</td>
                    <td scope="col">Sort</td>
                    <td scope="col">
                        <i class="fa fa-search"></i> Search</td>
                </tr>
            </thead>
            <tbody id="tbody">
                <tr>
                    <td>                    
                        <input type="checkbox">
                    </td>
                    <td>Nov. 7, 2019</td>
                    <th>
                        Write a Description
                        <p class="lead small">Cash on hand</p>
                    </th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: green;">Php0.00</td>
                    <td style="color: red;" colspan="2" class="small">Choose a category</td>
                    <td></td>
                    <td class="text-center">
                        <i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td>                    
                        <input type="checkbox">
                    </td>
                    <td>Nov. 7, 2019</td>
                    <th>
                        Write a Description
                        <p class="lead small">Cash on hand</p>
                    </th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: green;">Php0.00</td>
                    <td style="color: red;" colspan="2" class="small">Choose a category</td>
                    <td></td>
                    <td class="text-center">
                        <i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td>                    
                        <input type="checkbox">
                    </td>
                    <td>Nov. 7, 2019</td>
                    <th>
                        Write a Description
                        <p class="lead small">Cash on hand</p>
                    </th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: green;">Php0.00</td>
                    <td style="color: red;" colspan="2" class="small">Choose a category</td>
                    <td></td>
                    <td class="text-center">
                        <i class="fa fa-check"></i>
                    </td>
                </tr>
                                <tr>
                    <td>                    
                        <input type="checkbox">
                    </td>
                    <td>Nov. 7, 2019</td>
                    <th>
                        Write a Description
                        <p class="lead small">Cash on hand</p>
                    </th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: green;">Php0.00</td>
                    <td style="color: red;" colspan="2" class="small">Choose a category</td>
                    <td></td>
                    <td class="text-center">
                        <i class="fa fa-check"></i>
                    </td>
                </tr>  
            </tbody>
        </table>
        <!-- Table -->
         
    </div>
    <!-- Container -->
@endsection