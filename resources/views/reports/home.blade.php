@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <h1>Reports</h1>

    {{-- Bigger picture --}}
    <div class="card card-body mt-4 p-5">
        <div class="row">
            {{-- Left pane: Description --}}
            <div class="col-lg-6">
                <h3>Get the big picture</h3>
                <p class="lead">
                    How much profit are you making? Are your assets growing faster than your liabilities? 
                    Is cash flowing, or getting stuck?
                </p>
            </div>{{-- Left pane: Description --}}

            {{-- Right pane: Links --}}
            <div class="col-lg-6">

                {{-- Profit and loss link --}}
                <div>
                    <a href="" class="h3 text-primary">Profit and Loss (Income Statement)</a>
                    <p class="text-muted mt-2">Summary of your revenue and expenses that determine the profit you made.</p>    
                </div>{{-- Profit and loss link --}}
                
                <hr>

                {{-- Balance sheet --}}
                <div>
                    <a href="" class="h3 text-primary">Balance sheet</a>
                    <p class="text-muted mt-2">
                        Snapshot of what your business owns or is due to receive from others (assets), 
                        what it owes to others (liabilities), and what you've invested or retained in your company (equity).
                    </p>    
                </div>{{-- Balance sheet --}}

                <hr>

                {{-- Cash flow --}}
                <div>
                    <a href="" class="h3 text-primary">Cash flow</a>
                    <p class="text-muted mt-2">
                            Cash coming in and going out of your business. Includes items not included in Profit &amp; 
                            Loss such as repayment of loan principal and owner drawings (paying yourself).
                    </p>    
                </div>{{-- Cash flow --}}

            </div>{{-- Right pane: Links --}}
        </div>
    </div>{{-- Bigger picture --}}

    {{-- Taxes --}}
    <div class="card card-body mt-4 p-5">
        <div class="row">
            {{-- Left pane: Description --}}
            <div class="col-lg-6">
                <h3>Stay on top of taxes</h3>
                <p class="lead">
                    Find out how much tax you’ve collected and how much tax you owe back to tax agencies.
                </p>
            </div>{{-- Left pane: Description --}}

            {{-- Right pane: Links --}}
            <div class="col-lg-6">

                {{-- Sales tax --}}
                <div>
                    <a href="" class="h3 text-primary">Sales tax</a>
                    <p class="text-muted mt-2">Taxes collected from sales and paid on purchases to help you file sales tax returns.</p>    
                </div>{{-- Sales tax --}}
                
            </div>{{-- Right pane: Links --}}
        </div>
    </div>{{-- Taxes --}}

    {{-- Customers --}}
    <div class="card card-body mt-4 p-5">
        <div class="row">
            {{-- Left pane: Description --}}
            <div class="col-lg-6">
                <h3>Focus on customers</h3>
                <p class="lead">
                    See which customers contribute most of your revenue, and keep on top of overdue balances.
                </p>
            </div>{{-- Left pane: Description --}}

            {{-- Right pane: Links --}}
            <div class="col-lg-6">

                {{-- Income by customer --}}
                <div>
                    <a href="" class="h3 text-primary">Income by customer</a>
                    <p class="text-muted mt-2">Paid or unpaid income broken down by customer.</p>    
                </div>{{-- Income by customer --}}

                <hr>

                {{-- Aged receivables --}}
                <div>
                    <a href="" class="h3 text-primary">Aged receivables</a>
                    <p class="text-muted mt-2">Unpaid and overdue bills for the last 30, 60, and 90+ days.</p>    
                </div>{{-- Aged receivables --}}
                
            </div>{{-- Right pane: Links --}}
        </div>
    </div>{{-- Customers --}}

    {{-- Vendors --}}
    <div class="card card-body mt-4 p-5">
        <div class="row">
            {{-- Left pane: Description --}}
            <div class="col-lg-6">
                <h3>Focus on vendors</h3>
                <p class="lead">
                    Understand business spending, where you spend, and how much you owe to your vendors.
                </p>
            </div>{{-- Left pane: Description --}}

            {{-- Right pane: Links --}}
            <div class="col-lg-6">

                {{-- Purchases by vendor --}}
                <div>
                    <a href="" class="h3 text-primary">Purchases by vendor</a>
                    <p class="text-muted mt-2">Business purchases, broken down by who you bought from.</p>    
                </div>{{-- Purchases by vendor --}}

                <hr>
                
                {{-- Aged payables --}}
                <div>
                    <a href="" class="h3 text-primary">Aged payables</a>
                    <p class="text-muted mt-2">Unpaid and overdue bills for the last 30, 60, and 90+ days.</p>    
                </div>{{-- Aged payables --}}
                
            </div>{{-- Right pane: Links --}}
        </div>
    </div>{{-- Vendors --}}

    {{-- Dig deeper --}}
    <div class="card card-body mt-4 p-5">
        <div class="row">
            {{-- Left pane: Description --}}
            <div class="col-lg-6">
                <h3>Dig deeper</h3>
                <p class="lead">
                    Drill into the detail of financial transactions over the life of your company.
                </p>
            </div>{{-- Left pane: Description --}}

            {{-- Right pane: Links --}}
            <div class="col-lg-6">

                {{-- Account balances --}}
                <div>
                    <a href="" class="h3 text-primary">Account balances</a>
                    <p class="text-muted mt-2">Summary view of balances and activity for all accounts.</p>    
                </div>{{-- Account balances --}}

                <hr>
                
                {{-- Trial balance --}}
                <div>
                    <a href="" class="h3 text-primary">Trial balance</a>
                    <p class="text-muted mt-2">Balance of all your accounts on a specified date.</p>    
                </div>{{-- Trial balance --}}

                <hr>

                {{-- Account transactions (General ledger) --}}
                <div>
                    <a href="" class="h3 text-primary">Account transactions (General ledger)</a>
                    <p class="text-muted mt-2">Detailed list of all transactions and their total by account—everything in your Chart of Accounts.</p>    
                </div>{{-- Account transactions (General ledger) --}}
                
            </div>{{-- Right pane: Links --}}
        </div>
    </div>{{-- Dig deeper --}}
</div>
@endsection
