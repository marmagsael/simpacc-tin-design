@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row mb-4">
        <div class="col-lg-6 col-md-4">
            <h1>Profit and Loss</h1>
        </div>
        
        <div class="col-lg-6 col-md-8 d-sm-block">
            <div class="text-right button-header" >
                {{-- Export button --}}
                <div class="btn-group">
                    <button class="btn btn-white font-weight-bold btn-rounded px-4 mx-2 dropdown-toggle" data-flip="false" data-toggle="dropdown">Export</button>    
                    <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="">CSV</a>
                            <a class="dropdown-item" href="">PDF</a>
                    </div>
                </div>   
            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- User specification of date range --}}
    <form action="">
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <p class="lead">Date range</p>
                </div>
                <div class="col-lg-4 col-md-3">
                    <div class="row">
                        <div class="col-lg-3">
                            From:
                        </div>
                        <div class="col-lg-9">
                            <input type="date" class="form-control">   
                        </div> 
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 mb-3">
                    <div class="row">
                        <div class="col-lg-3">
                            To:
                        </div>
                        <div class="col-lg-9">
                            <input type="date" class="form-control">   
                        </div> 
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 text-right">
                    <button class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Update report</button>
                </div>
            </div>
        </div>    
    </form>{{-- User specification of date range --}}
    
    {{-- Computation of net profit --}}
    <div class="row mt-5">
        <div class="col-lg-2 col-md-2 text-center">
            <p>Income</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">-</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center">
            <p>Cost of Goods Sold</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">-</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center">
            <p>Operating Expenses</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">=</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center text-success">
            <p>Net Profit</p>
            <p class="h1">Php0.00</p>
        </div>
    </div>{{-- Computation of net profit --}}

    {{-- Sub navigation --}}
    <div class="col-md-3 mx-auto">
        <ul class="nav nav-pills nav-justified mt-4">
            <li class="nav-item">
                <a class="nav-link active" href="#summarySection" data-toggle="pill">Summary</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#detailsSection" data-toggle="pill">Details</a>
            </li>
        </ul>
    </div>
    {{-- Sub navigation --}}

    {{-- Main Content --}}
    <div class="tab-content" id="pills-tabContent">
        {{-- Summary --}}
        <div class="tab-pane fade show active" id="summarySection">
            <table class="table table-borderless mt-5">
                <thead>
                    <tr>
                        <th class="lead font-weight-bold">Accounts</th>
                        <th class="lead font-weight-bold text-right">Jan 01, 2019 to Nov 12, 2019</th>
                    </tr>
                </thead>
            </table>
            
            <table class="table">
                <tbody>
                    <tr>
                        <td class="lead">Income</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Cost of Goods Sold</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr class="bg-white">
                        <td class="lead">
                            <div class="font-weight-bold">Gross Profit</div>
                            <div class="text-muted">As a percentage of Total Income</div>
                        </td>
                        <td class="lead text-right">
                            <div class="font-weight-bold">PHP0.00</div>
                            <div class="text-muted">0.00%</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="lead">Operating Expenses</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr class="bg-white">
                        <td class="lead">
                            <div class="font-weight-bold">Net Profit</div>
                            <div class="text-muted">As a percentage of Total Income</div>
                        </td>
                        <td class="lead text-right">
                            <div class="font-weight-bold">PHP0.00</div>
                            <div class="text-muted">0.00%</div>
                        </td>
                    </tr>
                </tbody>
            </table>    
        </div>{{-- Summary --}}

        {{-- Details --}}
        <div class="tab-pane fade" id="detailsSection">
            <table class="table table-striped mt-5">
                <thead>
                    <tr>
                        <th class="lead font-weight-bold">Accounts</th>
                        <th class="lead font-weight-bold text-right">Jan 01, 2019 to Nov 12, 2019</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="lead">Total Income</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Total of Cost of Goods Sold</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">
                            <div class="font-weight-bold">Gross Profit</div>
                            <div class="text-muted">As a percentage of Total Income</div>
                        </td>
                        <td class="lead text-right">
                            <div class="font-weight-bold">PHP0.00</div>
                            <div class="text-muted">0.00%</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="lead">Total Operating Expenses</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">
                            <div class="font-weight-bold">Net Profit</div>
                            <div class="text-muted">As a percentage of Total Income</div>
                        </td>
                        <td class="lead text-right">
                            <div class="font-weight-bold">PHP0.00</div>
                            <div class="text-muted">0.00%</div>
                        </td>
                    </tr>
                </tbody>
            </table>    
        </div>{{-- Details --}}
    </div>{{-- Main Content --}}

</div>
@endsection
