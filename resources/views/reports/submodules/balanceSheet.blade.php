@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row mb-4">
        <div class="col-lg-6 col-md-4">
            <h1>Balance Sheet</h1>
        </div>
        
        <div class="col-lg-6 col-md-8 d-sm-block">
            <div class="text-right button-header" >
                {{-- Export button --}}
                <div class="btn-group">
                    <button class="btn btn-white font-weight-bold btn-rounded px-4 mx-2 dropdown-toggle" data-flip="false" data-toggle="dropdown">Export</button>    
                    <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="">CSV</a>
                            <a class="dropdown-item" href="">PDF</a>
                    </div>
                </div>   
            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- User specification of date range --}}
    <form action="">
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-1 col-md-2">
                    <p class="lead">As of</p>
                </div>
                <div class="col-lg-3 col-md-5 mb-3">
                    <div>
                        <input type="date" class="form-control">   
                    </div> 
                </div>
                <div class="col-lg-8 col-md-5 text-right">
                    <button class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Update report</button>
                </div>
            </div>
        </div>    
    </form>{{-- User specification of date range --}}

    {{-- Computation --}}
    <div class="row mt-5">
        <div class="col-lg-2 col-md-2 text-center">
            <p>Cash and Bank</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">-</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center">
            <p>To be received</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">-</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center">
            <p>To be paid out</p>
            <p class="h1">Php0.00</p>
        </div>
        <div class="col-lg-1 col-md-1 text-center">
            <p class="text-white">minus</p>
            <p class="h1">=</p>
        </div>
        <div class="col-lg-2 col-md-2 text-center text-success">
            <p></p>
            <p class="h1">Php0.00</p>
        </div>
    </div>{{-- Computation --}}

    {{-- Sub navigation --}}
    <div class="col-md-3 mx-auto">
        <ul class="nav nav-pills nav-justified mt-4">
            <li class="nav-item">
                <a class="nav-link active" href="#summarySection" data-toggle="pill">Summary</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#detailsSection" data-toggle="pill">Details</a>
            </li>
        </ul>
    </div>
    {{-- Sub navigation --}}

    {{-- Main Content --}}
    <div class="tab-content" id="pills-tabContent">

        {{-- Summary --}}
        <div class="tab-pane fade show active" id="summarySection">
            <table class="table table-borderless mt-5">
                <thead>
                    <tr>
                        <th class="lead font-weight-bold">Accounts</th>
                        <th class="lead font-weight-bold text-right">Date Today</th>
                    </tr>
                </thead>
            </table>
            
            {{-- Assets --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Assets</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="lead">Total Cash and Bank</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Total Other Current Assets</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Total Long-term Assets</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Assets</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Assets --}}

            {{-- Liabilities --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Liabilities</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="lead">Total Current Liabilities</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Total Long-term Liabilities</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Liabilities</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Liabilities --}}

            {{-- Equity --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Equity</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="lead">Total Other Equity</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead">Total Retained Earnings</td>
                        <td class="lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Equity</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Equity --}}

        </div>{{-- Summary --}}

        {{-- Details --}}
        <div class="tab-pane fade" id="detailsSection">
            <table class="table table-borderless mt-5">
                <thead>
                    <tr>
                        <th class="lead font-weight-bold">Accounts</th>
                        <th class="lead font-weight-bold text-right">Date Today</th>
                    </tr>
                </thead>
            </table>
            
            {{-- Assets --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Assets</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Cash and Bank</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Cash and Bank</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Other Current Assets</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Other Current Assets</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr  class="bg-white">
                        <td class="pl-5 lead">Long-term Assets</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Long-term Assets</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Assets</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Assets --}}

            {{-- Liabilities --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Liabilities</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Current Liabilities</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Current Liabilities</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Long-term Liabilities</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Long-term Liabilities</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Liabilities</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Liabilities --}}

            {{-- Equity --}}
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th class="lead font-weight-bold">Equity</th>
                        <th class="lead font-weight-bold text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Other Equity</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Other Equity</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr class="bg-white">
                        <td class="pl-5 lead">Retained Earnings</td>
                        <td class="pl-5 lead text-right"></td>
                    </tr>
                    <tr>
                        <td class="pl-5 lead">Total Retained Earnings</td>
                        <td class="pl-5 lead text-right">PHP0.00</td>
                    </tr>
                    <tr>
                        <td class="lead font-weight-bold">Total Equity</td>
                        <td class="lead text-right font-weight-bold">PHP0.00</td>
                    </tr>
                </tbody>
            </table>{{-- Equity --}} 
        </div>{{-- Details --}}

    </div>{{-- Main Content --}}

</div>
@endsection
