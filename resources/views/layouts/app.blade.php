<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/croppie.css') }}" rel="stylesheet">
    @yield('style')
    <link rel="gettext" type="application/x-po" href="{{asset('languages/en/LC_MESSAGES/en.po')}}" />
    
</head>
<body>
    <div id="app">
        {{-- Top Navigation Bar --}}
        <nav class="navbar fixed-top navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>{{-- Top Navigation Bar --}}

        @if(Auth::check())
            {{-- Main Page --}}
            <div id="main">
                {{-- Side Bar --}}
                <div id="sidebar" class="shadow-sm default-sidebar-width">
                    {{-- Side Bar Content --}}
                    <div id="sidebar-content">
                        {{-- Open and Close Side Bar Function --}}
                        <div id="sideBarTogglerWrapper">
                            <i class="btn btn-outline-dark fa fa-bars" id="sideBarToggler"></i>
                        </div>{{-- Open and Close Side Bar Function --}}

                        {{-- Side Bar Links --}}
                        <div class="sidebar-links">

                            {{-- Sales Module Link --}}
                            <div class="accordion side-link" id="accordion1">
                                <div>
                                    <a href="" class="nav-link" data-toggle="collapse" data-target="#content1">Sales</a>
                                </div>
                                <div id="content1" class="collapse" data-parent="#accordion1" >
                                    <a href="/sales/customer" class="nav-link" >Customer</a>
                                    <a href="/sales/products-and-services" class="nav-link" >Product and Services</a>
                                    <a href="/sales/invoices" class="nav-link" >Invoices</a>
                                    <a href="/sales/customer-statements" class="nav-link" >Customer Statements</a>
                                </div>
                            </div>{{-- Sales Module Link --}}

                            {{-- Purchases Module Link --}}
                            <div class="accordion side-link" id="accordion2">
                                <div>
                                    <a href="" class="nav-link" data-toggle="collapse" data-target="#content2">Purchases</a>
                                </div>
                                <div id="content2" class="collapse" data-parent="#accordion2" >
                                    <a href="/purchases/vendors" class="nav-link" >Vendor</a>
                                    <a href="/purchases/products-and-services" class="nav-link" >Product and Services</a>
                                    <a href="/purchases/bills" class="nav-link" >Bills</a>
                                    <a href="/purchases/receipts" class="nav-link" >Receipts</a>
                                </div>
                            </div>{{-- Purchases Module Link --}}

                            {{-- Accounting Module Link --}}
                            <div class="accordion side-link" id="accordion3">
                                <div>
                                    <a href="" class="nav-link" data-toggle="collapse" data-target="#content3">Accounting</a>
                                </div>
                                <div id="content3" class="collapse" data-parent="#accordion3" >
                                    <a href="" class="nav-link" >Transactions</a>
                                    <a href="/accounting/reconciliation" class="nav-link" >Reconciliation</a>
                                    <a href="/accounting/chart-of-accounts/create" class="nav-link" >Chart of Accounts</a>
                                </div>
                            </div>{{-- ACcounting Module Link --}}

                            {{-- Reports Module Link --}}
                            <div class="accordion side-link">
                                <a href="/reports" class="nav-link" >Reports</a>
                            </div>{{-- Reports Module Link --}}


                        </div>{{-- Side Bar Links --}}
                    </div>{{-- Side Bar Content --}}
                </div>{{-- Side Bar --}}  

                {{-- Page Content --}}
                <div id="content" class="default-content-width">
                    <div id="main-content">
                        @yield('page-content')
                    </div>
                </div>{{-- Page Content --}}

                @else
                <div class="default-content">
                    {{session('msg')}}
                    @yield('auth-content')
                </div>

            </div>{{-- Main Page --}}
        @endif

        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrapSelect/bootstrap-bundle.js') }}"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/sidebar.js') }}"></script>
        @yield('script')
    </div>
</body>
</html>
