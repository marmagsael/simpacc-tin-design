@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/bills/style.css') }}" rel="stylesheet">
@endsection

@section('page-content')
<div class="container">

    <!--- HEADER --->
        <div class="d-flex flex-row">
            <h1>Bills</h1>
            <div class="ml-auto">
                <div class="btn-group">
                    <a class="button btn btn-primary font-weight-bold btn-rounded px-4" href="/purchases/bills/create">
                        Create a bill</a>
                </div>
            </div>
        </div>
    <!--END OF HEADER -->

    <!-- DATE -->
        <div class="row mt-4">
            <div class="col-lg-2 col-md-3 col-sm-10">
                <select class="form-control form-control">
                    <option>All Vendors</option>
                </select>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-10 date-element">
                    <input type="date" class="form-control" id="From" placeholder="From">
            </div>
            <div class="col-lg-1 col-md-1 col-sm-10 date-element">
                <p class="text-center">to</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-10 date-element">
                    <input type="date" class="form-control" id="To" placeholder="To">
            </div>
        </div>
    <!-- END OF DATE -->
    <!-- TABLE -->
            <table class="table mt-5">
                <thead class="thead-light">
                    <tr>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Vendor</th>
                        <th>Amount Due</th>
                        <th>Payment Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
    <!-- END OF TABLE -->

    <!-- CREATE A PAGE -->
            <table class="table table-bordered">
                <thead class="thead-light text-primary py-5">
                    <tr class="table-info">
                        <th><p class="card-text ml-5">You do not have any bills. Why not <a href="#"><u>create a bill</u></a>?</p></th>
                    </tr>
                </thead>
            </table>
    <!-- CREATE A PAGE -->

    <div class="row mt-5">
        <div class="col-lg-1 col-md-1 col-sm-1">
            <p class="text-right">Show:</p>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
                <select class="form-control form-control-md"> 
                    <option>5</option>
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                </select>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
            <p class="text-left">per page</p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
        
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="row">
                <p>Page 1 of 1 </p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination rounded ml-2">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                            </li>
                                <a class="page-link" href="#" aria-label="Next">
                                    <i class="fa fa-caret-right"></i>    
                                </a>
                            </li>
                        </ul>
                    </nav>
            </div>
        </div>
        
        <div class="col-lg-2 col-md-2 col-sm-2">
            <p class="text-right">Jump to Page</p>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
                <select class="form-control form-control-md"> 
                    <option></option>
                    <option>1</option>
                    <option>Jump to Page</option>
                </select>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
            <p class="text-left"></p>
        </div>

    </div>
</div>
@endsection