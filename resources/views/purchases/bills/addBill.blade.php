@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="d-flex flex-row">
            <h1>Add Bill</h1>
    </div>

    <!-- START OF ROW -->
    <div class="row">
        <!-- VENDOR/CURRENCY -->
                    <div class="col-lg-4 col-md-4 col-sm-4 flex-column mt-5">
                        <form class="justify-content-start">
                            <div class="form-group row">
                                        <p class="col lead text-right">Vendor</p>
                                <div class="col">
                                    <div class="dropdown">
                                        <button class="btn btn-light btn-md dropdown-toggle" type="button" id="vendor" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Choose
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="vendor">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#"></a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">
                                            <i class="fa fa-plus-circle fa-sm"></i>
                                                Add Vendor
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                    <p class="col lead text-right">Currency</p>
                                <div class="col">
                                    <div class="dropdown">
                                        <button class="btn btn-light btn-md dropdown-toggle" type="button" id="vendor" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Choose
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="vendor">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
        <!-- END OF VENDOR/CURRENCY -->

        <!-- DATE/DUEDATE/PO/SO -->
                        <div class="col-lg-4 col-md-4 col-sm-4 mt-5">
                            <form class="">
                                <div class="form-group row">
                                    <p class="col lead text-right">Date</p>
                                    <div class="col">
                                        <input type="date" class="form-control" id="Invoicenumber">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p class="col lead text-right">Due Date</p>
                                    <div class="col">
                                        <input type="date" class="form-control" id="POnumber">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p class="col-sm-5 lead text-right">P.O/S.O</p>
                                    <div class="col">
                                        <input type="text" class="form-control" id="POnumber">
                                    </div>
                                </div>
                            </form>
                        </div>
        <!-- END OF DATE/DUEDATE/PO/SO -->

        <!-- BILL/NOTES -->
                        <div class="col-lg-4 col-md-4 col-sm-4 mt-5">
                            <form class="">
                                <div class="form-group row">
                                    <p class="col lead text-right">Bill #</p>
                                    <div class="col">
                                        <input type="date" class="form-control" id="Invoicenumber">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p class="col lead text-right">Notes   </p>
                                    <div class="col">
                                        <textarea class="form-control" id="textarea" rows="5" width="5"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
        <!-- END OF BILL/NOTES -->

    </div>
    <!-- END OF ROW -->

    <div class="card">
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>Item</th>
                    <th>Expense Category</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Price</th>
                    <th class="">Tax</th>
                    <th class="text-center">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-light btn-md dropdown-toggle" type="button" id="vendor" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Choose
                            </button>
                            <div class="dropdown-menu" aria-labelledby="vendor">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#"></a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                <i class="fa fa-plus-circle fa-sm"></i>
                                    Add New Product
                                </a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-light btn-md dropdown-toggle" type="button" id="vendor" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Choose
                            </button>
                            <div class="dropdown-menu" aria-labelledby="vendor">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#"></a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                <i class="fa fa-plus-circle fa-sm"></i>
                                    Add New Product
                                </a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <textarea class="form-control" id="textarea" rows="1"></textarea>
                    </td>
                    <td>
                        <input type="text" class="form-control">
                    </td>
                    <td>
                        <input type="text" class="form-control ">
                    </td>
                    <td>
                        <input type="text" class="form-control">
                    </td>
                    <td>
                        <p>Php0.00</p>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">Subtotal:</td>
                    <td>Php0.00</td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">Total:</td>
                    <td>Php0.00</td>
                </tr>
            </tbody>
        </table>
        <div class="modal-footer">
            <button type="button" class="btn btn-rounded btn-light">Cancel</button>
            <button type="button" class="btn btn-rounded btn-primary">Save</button>
        </div>
    </div>

</div>
@endsection