@extends('layouts.app')

@section('page-content')
<div class="container">
    <!--- HEADER --->
    <div class="d-flex flex-row">
        <h1>Receipts</h1>
        <div class="ml-auto">
            <div class="btn-group">
                <button class="button btn btn-primary font-weight-bold btn-rounded px-4">
                    Upload a receipt
                </button>
            </div>
        </div>
    </div>
    <!-- HEADER -->

    <!-- LEARN MORE TABLE -->
    <div class="card py-3 mt-5">
        <div class="row">
            <div class="col-lg-9">
                <p class="card-text ml-5">
                    <b>Did you know?</b> You can also submit receipts by emailing them to
                    <a href="#" class="font-weight-bold text-black">receipts@waveapps.com</a>or
                    with our <b>IOS</b> or <b>Android</b> app.
                </p>  
            </div>
            <div class="col-lg-3 text-right pr-5">
                <button class="button btn btn-light border-primary text-primary font-weight-bold btn-rounded px-4">
                    Learn More
                </button>
            </div>
        </div>
    </div>
    <!-- LEARN MORE TABLE -->

    <!-- PROCESS -->
    <ul class="nav mt-5 text-black">
        <li class="nav-item">
            <a href="#" class="nav-link active" id="AllStatuses"> All Statuses </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link active" id="Processing"> Processing </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link active" id="Ready"> Ready </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link active" id="Done"> Done </a>
        </li>
    </ul>
    <!-- PROCESS -->

    <!-- TABLE -->
    <table class="table">
        <thead>
            <tr>
                <th>Status</th>
                <th>Date</th>
                <th>Merchant</th>
                <th>Category</th>
                <th>Account</th>
                <th>Source</th>
                <th></th>
                <th></th>
                <th class="text-right">Total</th>
                <th class="text-right">Actions</th>   
            </tr>
        </thead>
    </table>
    <!-- TABLE -->

    <!-- CARD BODY -->
    <div class="card-body text-center">
        <div class="col mt-5">
            <p class="font-weight-bold">
                There are no receipts to display.
            </p>
        </div>
        <div class="col">
            Upload a receipt using the button above.
        </div>
    </div>
    <!-- CARD-BODY -->

    <!-- PAGINATION -->
    <div class="row mt-5 ml-5">
            <p class="text-right">Show:</p>
        <div class="col-lg-1 col-md-1 col-sm-1">
                <select class="form-control form-control-md"> 
                    <option>5</option>
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                </select>
        </div>
            <p class="text-left">per page</p>


        <div class="col-lg-7 col-md-4 col-sm-4">
        
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="row">
                <p>Page 1 of 1 </p>
                    <nav aria-label="Page nav">
                        <ul class="pagination rounded ml-2">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                            </li>
                                <a class="page-link" href="#" aria-label="Next">
                                    <i class="fa fa-caret-right"></i>    
                                </a>
                            </li>
                        </ul>
                    </nav>
            </div>
        </div>
    </div>
    <!-- PAGINATION -->

</div>
@endsection