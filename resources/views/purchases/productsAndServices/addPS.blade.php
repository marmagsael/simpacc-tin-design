@extends('layouts.app')

@section('page-content')
    <div class="container">
        <h1>Add a Product or Service</h1>
        <p class="lead">Products and services that you buy from vendors are used as items on Bills to record those purchases, 
            and the ones that you sell to customers are used as items on Invoices to record those sales.
        </p>

        <div class="my-4">
            <form action="">

                {{-- Fields --}}
                <div class="my-2">

                    <div class="col-lg-7 col-md-9 mx-auto">

                        {{-- Name --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Name <i class="text-danger">*</i></p></div>
                            <div class="col-lg-9 col-md-8"><input type="text" class="form-control my-2"></div>
                        </div>{{-- Name --}}
        
                        {{-- Description --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Description</p></div>
                            <div class="col-lg-9 col-md-8">
                                <textarea class="form-control" rows="10"></textarea>
                            </div>
                        </div>{{-- Description --}}    
        
                        {{-- Price --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Price</p></div>
                            <div class="col-lg-9 col-md-8"><input type="text" class="form-control my-2"></div>
                        </div>{{-- Price --}}
                        
                        {{-- Sell this --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Sell this</p></div>
                            <div class="col-lg-9 col-md-8">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="sellThisCheckBox">
                                    <label class="custom-control-label font-italic" for="sellThisCheckBox">Allow this product or service to be added to Invoices.</label>
                                </div>
                            </div>
                        </div>{{-- Sell this --}}

                        {{-- Buy this --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Buy this</p></div>
                            <div class="col-lg-9 col-md-8">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="buyThisCheckBox">
                                    <label class="custom-control-label font-italic" for="buyThisCheckBox">Allow this product or service to be added to Bills.</label>
                                </div>
                            </div>
                        </div>{{-- Buy this --}}

                        {{-- Sales Tax --}}
                        <div class="row">
                            <div class="col-lg-3 col-md-4 text-lg-right text-md-right"><p class="lead my-2">Sales tax</p></div>
                            <div class="col-lg-9 col-md-8"><input type="text" class="form-control my-2"></div>
                        </div>{{-- Sales Tax --}}

                    </div> 
                    
                </div>{{-- Fields --}}

                {{-- Submit button --}}
                <div class="col-lg-6 col-md-9 mx-auto">
                    <input type="submit" value="Save" class="btn btn-primary btn-rounded float-right my-5">
                </div>{{-- Submit button --}}

            </form>    
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/sales/customer/customer.js') }}"></script>
@endsection