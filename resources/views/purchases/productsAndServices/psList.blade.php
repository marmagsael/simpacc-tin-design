@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <h1>Products and Services (Purchases)</h1>
        </div>
        <div class="col-lg-6 col-md-6 d-sm-block">
            <div class="text-lg-right text-md-right text-sm-center button-header">

                {{-- Add products and services button --}}
                <a href="/purchases/products-and-services/create" class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Add a product or service</a>

            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- Products and Services list --}}
    <div class="my-4">
        
        {{-- @if(table is not empty) --}}

        {{-- @else --}}
            <p class="font-italic">You haven't added any products yet.</p>
        {{-- @endif --}}  

    </div>{{-- Products and Services --}}
    
</div>
@endsection