@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="row">
        <div class="col-lg-6 col-md-4">
            <h1>Vendors</h1>
        </div>
        
        <div class="col-lg-6 col-md-8 d-sm-block">
            <div class="text-lg-right text-md-right text-sm-center button-header" >
                {{-- Import button --}}
                <div class="btn-group">
                    <button class="btn btn-white font-weight-bold btn-rounded px-4 mx-2 dropdown-toggle" data-flip="false" data-toggle="dropdown">Import from...</button>    
                    <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="/vendors/import-from-csv">CSV</a>
                            <a class="dropdown-item" href="/vendors/import-from-google">Google Contacts</a>
                    </div>
                </div>

                {{-- Add customer button --}}
                <a href="/purchases/vendors/create" class="btn btn-primary font-weight-bold btn-rounded px-4 mx-2">Add a vendor</a>    
            </div>
        </div>
            
    </div>{{-- Header --}}

    {{-- Vendor list --}}
    <div class="my-4">
        
        {{-- @if(table is not empty) --}}

        {{-- @else --}}
            <p class="font-italic">You haven't added any vendors yet.</p>
        {{-- @endif --}}  

    </div>{{-- Vendor list --}}
    
</div>
@endsection