@extends('layouts.app')

@section('page-content')
<div class="container">
    {{-- Header --}}
    <div class="d-flex flex-column">

        <h1>Import Vendors from CSV</h1>
        <p class="lead">A CSV (comma-separated values) file is a spreadsheet file that is used by Wave to import customer information into your business.</p>
            
    </div>{{-- Header --}}

    {{-- File Upload --}}
    <form action="">
        <div class="col-lg-6 col-md-7 my-5 ">
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile">
                    <label class="custom-file-label" for="inputGroupFile">Choose file</label>
                </div>
            </div>
            <p class="font-italic">Maximum 10MB file size. CSV file type only.</p>
            <a href="" class="btn btn-primary font-weight-bold btn-rounded px-4 mx-auto">Import Vendors from CSV</a>
        </div>    
    </form>{{-- File Upload --}}
    
    <hr class="my-5">

    <h2>Need help creating your CSV file?</h2>

    {{-- Instructions --}}
    <div class="my-5">
        <h3>Vendors CSV template file</h3>
        <p>Download and view our vendors CSV template. You can use this as a template for creating your CSV file.</p>

        <h3 class="mt-5">File Format</h3>
        <p>The first line of your vendors CSV must include all of the headers listed below, which are included in the vendors CSV template.</p>
    
        {{-- File Format --}}
        <div class="card card-body">
            <p class="lead"><span class="font-weight-bold">Reminder </span> : All CSV file headers are case-sensitive.</p>
            
            <hr class="my-3">

            {{-- Company name --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Company Name</p></div>
                <div class="col-lg-10 col-md-8"><p>The name of the company. If blank, it will default to the vendor's first and last names.</p></div>
            </div>{{-- Company name --}}

            {{-- First name --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">First Name</p></div>
                <div class="col-lg-10 col-md-8"><p>The first name of your vendor.</p></div>
            </div>{{-- First name --}}

            {{-- Last name --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Last Name</p></div>
                <div class="col-lg-10 col-md-8"><p>The last name of your vendor.</p></div>
            </div>{{-- Last name --}}

            {{-- Email --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Email</p></div>
                <div class="col-lg-10 col-md-8"><p>The email address of your vendor.</p></div>
            </div>{{-- Email --}}

            {{-- Phone --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Phone</p></div>
                <div class="col-lg-10 col-md-8"><p>The vendor's phone number.</p></div>
            </div>{{-- Phone --}}

            {{-- Address 1 --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Address 1</p></div>
                <div class="col-lg-10 col-md-8"><p>The first line of the vendor's address.</p></div>
            </div>{{-- Address 1 --}}

            {{-- Address 2 --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Address 2</p></div>
                <div class="col-lg-10 col-md-8"><p>The second line of the vendor's address.</p></div>
            </div>{{-- Address 2 --}}

            {{-- City --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">City</p></div>
                <div class="col-lg-10 col-md-8"><p>The city the vendor is in.</p></div>
            </div>{{-- City --}}

            {{-- Postal Code --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Postal Code</p></div>
                <div class="col-lg-10 col-md-8"><p>The ZIP or postal code for the vendor's address.</p></div>
            </div>{{-- Postal Code --}}

            {{-- Country --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Country</p></div>
                <div class="col-lg-10 col-md-8"><p>The vendor's country ISO code.</p></div>
            </div>{{-- Country --}}

            {{-- Currency --}}
            <div class="row">
                <div class="col-lg-2 col-md-4"><p class="font-weight-bold">Currency</p></div>
                <div class="col-lg-10 col-md-8"><p>The currency ISO code for the vendor. If not specified, it will default to your business currency.</p></div>
            </div>{{-- Currency --}}
        </div>{{-- File Format --}}

        <h3 class="mt-5">Exporting your CSV file from Excel or other software</h3>
        <p class="lead"><span class="font-weight-bold">Reminder </span> : When importing vendor information, your CSV file must be in UTF-8 format.</p>
        <p>
            You can convert an Excel worksheet (such as the vendor CSV template) to a text file by using the Save As command. 
            In the Save as type… box, choose the CSV (Comma delimited) text file format for the worksheet.
        </p>
        <p>
            Most spreadsheet applications have the ability to save CSV files in UTF-8 format with the Save As... or Export command, 
            depending on the program.
        </p>
    </div> {{-- Instructions --}}
    
</div>
@endsection