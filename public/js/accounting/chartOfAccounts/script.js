$(document).ready(function(){

    $('#inputCurrency').hide();

    $('#selectAccountType').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var selected = $(this).val();
        var noCurrency = ["Expected Payments from Customers", "Depreciation and Amortization", "Due For Payroll", "Income", "Discount",
                            "Other income", "Operating Expense", "Cost of Goods Sold", "Payment Processing Fee", "Payroll Expense",
                            "Retained Earnings: Profit"];
        var status = false;
        var limit = 0;
        for(var i=0; i < noCurrency.length && limit <= 1; i++){
            if(selected == noCurrency[i]){
                status = true;
                limit++;
            }
        }

        if(status != true){
            $('#inputCurrency').show();
        }
        else{
            $('#inputCurrency').hide();
        }
        
    });
      
});