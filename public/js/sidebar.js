$(document).ready(function(){
    $('#sideBarToggler').click(function(){
        if($('#sidebar').hasClass('default-sidebar-width')){
            $('#sidebar').removeClass('default-sidebar-width');
            $('#sidebar').addClass('slim-sidebar-width');
            $('#content').removeClass('default-content-width');
            $('#content').addClass('wide-content-width');
            
        }
        else{
            $('#sidebar').removeClass('slim-sidebar-width');
            $('#sidebar').addClass('default-sidebar-width');
            $('#content').removeClass('wide-content-width');
            $('#content').addClass('default-content-width');
        }
   
    });
    $(window).resize(function(){
        var windowSize = $(window).width();
        if(windowSize < 900){
            $('#sidebar').removeClass('default-sidebar-width');
            $('#sidebar').addClass('slim-sidebar-width');
            $('#content').removeClass('default-content-width');
            $('#content').addClass('wide-content-width');    
        }
        else{
            $('#sidebar').removeClass('slim-sidebar-width');
            $('#sidebar').addClass('default-sidebar-width');
            $('#content').removeClass('wide-content-width');
            $('#content').addClass('default-content-width');
        }
    });
    var windowSize = $(window).width();
    if(windowSize < 900){
        $('#sidebar').removeClass('default-sidebar-width');
        $('#sidebar').addClass('slim-sidebar-width');
        $('#content').removeClass('default-content-width');
        $('#content').addClass('wide-content-width');    
    }
    else{
        $('#sidebar').removeClass('slim-sidebar-width');
        $('#sidebar').addClass('default-sidebar-width');
        $('#content').removeClass('wide-content-width');
        $('#content').addClass('default-content-width');
    }
    

});