$(document).ready(function(){

    // Add a customer section

    $('#shippingForm').hide();

    $('#shippingCheckBox').on('click',function () {
        if ($('#shippingCheckBox').is(':checked')) {
            $('#shippingForm').show();
        } else {
            $('#shippingForm').hide();
        }
    });

});